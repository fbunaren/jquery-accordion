from django.test import TestCase, LiveServerTestCase
from django.test import Client
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from django.apps import apps
import urllib.request
from time import sleep

# Create your tests here.
class AppTest(TestCase):
    def test_index_response(self):
        response = Client().get('/index.html')
        self.assertEqual(response.status_code,200)
    
    def test_index_template(self):
        response = Client().get('/index.html')
        self.assertTemplateUsed(response,'index.html')

class FunctionalTest(LiveServerTestCase):
    link = 'http://jquery-accordion.herokuapp.com/'

    def setUp(self):
        super().setUp()
        opt = webdriver.ChromeOptions()
        opt.add_argument('--no-sandbox')
        opt.add_argument('--headless')
        opt.add_argument('--disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(chrome_options=opt,executable_path='chromedriver')

    def tearDown(self):
        self.selenium.refresh()
        self.selenium.quit()
        super().tearDown()

    def test_index_loaded(self):
        self.selenium.get(self.link)
        #Test if the Webpage is Loaded
        self.assertTrue(self.selenium.find_elements_by_tag_name('img') != None)
        
    def test_accordion_tabs(self):
        self.selenium.get(self.link)
    
        accordion = self.selenium.find_element_by_id('accordion')
        elements = accordion.find_elements_by_tag_name('div')

        accordion_strings = ["Foundations of Programming 2 Teaching Assistant","Cyber Community","PJ LCT Cyber Competition","3rd Place, IPSC Ristek , Competitive Programming Contest"]

        for k,i in enumerate(accordion_strings):
            accordion.find_elements_by_tag_name('h3')[k].click()
            sleep(1)
            self.assertIn(i,elements[k].text)
    
    def test_accordion_up_down(self):
        self.selenium.get(self.link)
    
        accordion = self.selenium.find_element_by_id('accordion')
        btnUP = accordion.find_elements_by_name('btnUP')
        btnDOWN = accordion.find_elements_by_name('btnDOWN')

        headers = ["Activities","Organizational","Committee","Achievements"]
        for k in range(len(btnDOWN)):
            if k != 0:
                btnUP[k].click()
            else:
                btnDOWN[k].click()
            accordion = self.selenium.find_element_by_id('accordion')
            elements = accordion.find_elements_by_tag_name('div')
            sleep(1)
            self.assertNotIn(headers[k],accordion.find_elements_by_tag_name('h3')[k].text)
            if k != 0:
                btnDOWN[k].click()
            else:
                btnUP[k].click()
            accordion = self.selenium.find_element_by_id('accordion')
            elements = accordion.find_elements_by_tag_name('div')
            sleep(1)
            self.assertIn(headers[k],accordion.find_elements_by_tag_name('h3')[k].text)